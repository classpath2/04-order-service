# Kafka Commands

## Create topic
```
 bin/kafka-topics.sh --create --topic test-producer --bootstrap-server <broker-id>:9092
```
## List all the topics
`
bin/kafka-topics.sh --list --bootstrap-server <broker-id>:9092
`

## Describe topic
`
bin/kafka-topics.sh --describe --topic test-producer --bootstrap-server <broker-id>:9092
`

## Produce message
`
bin/kafka-console-producer.sh --topic test-producer --bootstrap-server <broker-id>:9092
`

## Consume message
`
bin/kafka-console-consumer.sh --topic test-producer --from-beginning --bootstrap-server <broker-id>:9092
`

## Delete topic
`
bin/kafka-topics.sh --delete --topic test-producer --bootstrap-server <broker-id>:9092
`

## Consuming messages

`
bin/kafka-console-consumer.sh --topic nse-stock-may --bootstrap-server <broker-id>:9092
`

## Lab-4 Demo-project

1. Update the AppConfig with the below configuration
```java
    public final static String topicName = "orders";
    public final static int noOfProducers = 2;
    public final static int producerSpeed = 100;
```
2. Load the configuration in PosSimulator class
```java
    String topicName = AppConfig.topicName;
    int noOfProducers = AppConfig.noOfProducers;
    int produceSpeed = AppConfig.producerSpeed;
    Properties properties = new Properties();
    properties.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfig.applicationID);
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

```
3. Create a ThreadPool to be executed by the executor
```java
   ExecutorService executor = Executors.newFixedThreadPool(noOfProducers);
```
4. Submit the task to Executor
```java
   RunnableProducer runnableProducer = new RunnableProducer(i, kafkaProducer, topicName, produceSpeed);
   runnableProducers.add(runnableProducer);
   executor.submit(runnableProducer);

```
5. In the RunnableProducer class `run` method add the below code 
```java
        try {
            logger.info("Starting producer thread - " + id);
            while (!stopper.get()) {
                PosInvoice posInvoice = invoiceGenerator.getNextInvoice();
                producer.send(new ProducerRecord<>(topicName, posInvoice.getStoreID(), posInvoice));
                Thread.sleep(produceSpeed);
            }
        } catch (Exception e) {
            logger.error("Exception in Producer thread - " + id);
            throw new RuntimeException(e);
        }
```
6. Test the topics
```
bin/kafka-console-consumer.sh --topic orders --bootstrap-server <broker-id>:9092 
```
