package io.classpath.kafka.orders.config;

public class AppConfig {
    public final static String applicationID = "PosSimulator";
    public final static String bootstrapServers = "<broker-1>:9092,<broker-2>:9092,<broker-3>:9092";
    public final static String topicName = "orders";
    public final static int noOfProducers = 2;
    public final static int producerSpeed = 100;
}
